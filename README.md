# Base container for Nginx #

- This container is configured to use Nginx for PHP Projects and Symfony Framework (see "domain.conf").
- The container has an user "docker" with UID "1000" to avoid permissions issues when working with "data volume"