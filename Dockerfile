# Nginx from DotDeb.org
#
# Version   1.0.0

FROM        mariorez/nginx:latest
MAINTAINER  Mario Rezende <mariorez@gmail.com>

COPY ./domain-dev.conf /etc/nginx/sites-available/

COPY ./domain-prod.conf /etc/nginx/sites-available/

RUN ln -nfs /etc/nginx/sites-available/domain-dev.conf /etc/nginx/sites-enabled/domain-dev.conf \
    && ln -nfs /etc/nginx/sites-available/domain-prod.conf /etc/nginx/sites-enabled/domain-prod.conf \
    && rm sites-enabled/domain.conf
